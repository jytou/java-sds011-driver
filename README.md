A Java driver for the SDS011 air quality sensor.

Loosely inspired from [this python code](https://gist.github.com/kadamski/92653913a53baf9dd1a8)

Also see the sensor specification [here](https://cdn-reichelt.de/documents/datenblatt/X200/SDS011-DATASHEET.pdf)

This also uses the [jSerialComm library](https://fazecast.github.io/jSerialComm/).

See usage in the main Driver class.

Typical usage:
```
		final SDS011Driver driver = new SDS011Driver();
		// wake up the device
		driver.setSleepMode(false);
		// retrieve firmware information
		final FirmwareInfo firmwareInfo = driver.getFirmwareInfo();
		System.out.println(String.format("Firmware: %02d-%02d-%02d, ID: %s", firmwareInfo.year, firmwareInfo.month, firmwareInfo.day, firmwareInfo.id));
		// get ready to retrieve data
		driver.setWorkingPeriod(PERIOD_CONTINUOUS);
		// start retrieving data mode
		driver.setMode(MODE_QUERY);
		// getting data loop
		while (true)
		{
			System.out.println("Waking up...");
			// wait for the device to warm up
			Thread.sleep(5000);
			// collect 5 samples
			for (int i = 0; i < 5; i++)
			{
				final double[] values = driver.queryData();
				System.out.println(String.format("PM2.5: %.1f, PM10: %.1f", values[0], values[1]));
				Thread.sleep(2000);
			}
			// put back device to sleep
			driver.setSleepMode(true);
			// it is best to make it sleep as much as possible to preserve its life - 8000 hours lifetime according to manufacturer
			System.out.println("Sleeping for 5 minutes");
			Thread.sleep(60000 * 5);
		}
```
