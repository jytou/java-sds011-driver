package jyt.driver.sds011;

public class SDS011CommunicationException extends Exception
{
	public SDS011CommunicationException(String pMessage, Throwable pCause, boolean pEnableSuppression, boolean pWritableStackTrace)
	{
		super(pMessage, pCause, pEnableSuppression, pWritableStackTrace);
	}

	public SDS011CommunicationException(String pMessage, Throwable pCause)
	{
		super(pMessage, pCause);
	}

	public SDS011CommunicationException(String pMessage)
	{
		super(pMessage);
	}

	public SDS011CommunicationException(Throwable pCause)
	{
		super(pCause);
	}
}
