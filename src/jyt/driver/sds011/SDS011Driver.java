package jyt.driver.sds011;

import javax.naming.CommunicationException;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortInvalidPortException;

public class SDS011Driver implements AutoCloseable
{
	private final static int BAUD_RATE = 9600;
	public final static String DEFAULT_PORT = "/dev/ttyUSB0";

	public final static byte CMD_MODE = 2;
	public final static byte CMD_QUERY_DATA = 4;
	public final static byte CMD_DEVICE_ID = 5;
	public final static byte CMD_SLEEP = 6;
	public final static byte CMD_FIRMWARE = 7;
	public final static byte CMD_WORKING_PERIOD = 8;
	public final static byte MODE_ACTIVE = 0;
	public final static byte MODE_QUERY = 1;
	public final static byte PERIOD_CONTINUOUS = 0;

	public static class FirmwareInfo
	{
		public int year;
		public int month;
		public int day;
		public String id;

		public FirmwareInfo(int pYear, int pMonth, int pDay, String pId)
		{
			super();
			year = pYear;
			month = pMonth;
			day = pDay;
			id = pId;
		}
	}

	private SerialPort mPort;
	private boolean mSleeping = true;
	private String mPortString;

	public SDS011Driver() throws SDS011CommunicationException, SerialPortInvalidPortException
	{
		this(DEFAULT_PORT);
	}

	public SDS011Driver(String pPort) throws SDS011CommunicationException, SerialPortInvalidPortException
	{
		super();
		mPortString = pPort;
		connect();
	}

	private void connect() throws SDS011CommunicationException, SerialPortInvalidPortException
	{
		mPort = SerialPort.getCommPort(mPortString);
		mPort.setBaudRate(BAUD_RATE);
		if (mPort.openPort(5000))// sleep at least 5000 ms before opening the port, otherwise fail
			mPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 10, 0);
		else
			throw new SDS011CommunicationException("Waited too long to open port");
	}

	public void reconnect() throws SerialPortInvalidPortException, SDS011CommunicationException
	{
		close();
		connect();
	}

	private byte[] buildCommand(byte pCommand, byte[] pData)
	{
		final byte[] res = new byte[20];
		int ires = 0;
		res[ires++] = (byte)0xaa;
		res[ires++] = (byte)0xb4;
		res[ires++] = pCommand;
		int sum = pCommand - 2;
		for (int i = 0; i < pData.length; i++)
		{
			sum += pData[i] & 0xff;
			res[ires++] = pData[i];
		}
		ires = 3 + 12;// no matter what, the data cannot be more than 12 bytes
						// long
		res[ires++] = (byte)0xff;
		res[ires++] = (byte)0xff;
		res[ires++] = (byte)(sum % 256);
		res[ires++] = (byte)0xab;
		return res;
	}

	private double[] readData(byte[] pData) throws CommunicationException
	{
		if ((byte)(pData[0] & 0xff) != (byte)0xaa)
			throw new CommunicationException("First byte was 0x" + Integer.toHexString(pData[0] & 0xff) + " instead of 0xaa");
		if ((byte)(pData[1] & 0xff) != (byte)0xc0)
			throw new CommunicationException("Second byte was 0x" + Integer.toHexString(pData[1] & 0xff) + " instead of 0xc0");
		// read first data: 2.5 value then 10
		final int val25Int = (pData[2] & 0xff) + (pData[3] & 0xff) * 256;
		final int val10Int = (pData[4] & 0xff) + (pData[5] & 0xff) * 256;
		// System.out.println("Data: " + pData[2] + ", " + pData[3] + ", " +
		// pData[4] + ", " + pData[5]);
		// int values are ten times the actual double values
		final double[] result = new double[2];
		result[0] = 0.1 * val25Int;
		result[1] = 0.1 * val10Int;

		// read and check checksum
		final int checksumByte = pData[8] & 0xff;
		final int chk = calculateChecksum(pData);
		if (checksumByte != chk)
			throw new CommunicationException("Wrong checksum, expected " + chk + " but got " + checksumByte);

		// read confirmation code
		final int confirmationByte = pData[9] & 0xff;
		if (confirmationByte != 0xab)
			throw new CommunicationException("Confirmation byte was 0x" + Integer.toHexString(pData[9] & 0xff) + " instead of 0xab");

		return result;
	}

	private FirmwareInfo readFirmwareInfo(byte[] pData) throws CommunicationException
	{
		final int year = pData[3] & 0xff;
		final int month = pData[4] & 0xff;
		final int day = pData[5] & 0xff;

		final int idAsInt = (pData[6] & 0xff) + (pData[7] & 0xff) * 256;

		final int checksumByte = pData[8] & 0xff;
		final int chk = calculateChecksum(pData);
		if (checksumByte != chk)
			throw new CommunicationException("Wrong checksum, expected " + chk + " but got " + checksumByte);
		final int confirmationByte = pData[9] & 0xff;
		if (confirmationByte != 0xab)
			throw new CommunicationException("Confirmation byte was 0x" + Integer.toHexString(pData[9] & 0xff) + " instead of 0xab");
		return new FirmwareInfo(year, month, day, Integer.toHexString(idAsInt));
	}

	private int calculateChecksum(byte[] pData)
	{
		int chk = 0;
		for (int i = 2; i < 8; i++)
			chk += (pData[i] & 0xff);
		chk %= 256;
		return chk;
	}

	private byte[] readResponse() throws CommunicationException
	{
		byte[] buf = new byte[10];
		int c = 0;
		while (buf[0] != (byte)0xaa)
		{
			mPort.readBytes(buf, 1);
			// System.out.println("Read 0x" + Integer.toHexString(buf[0] & 0xFF));
			if (c++ > BAUD_RATE)
			// with this hardware, we shouldn't be waiting for more than 1 second
				throw new CommunicationException("Timeout while reading a response");
		}
		mPort.readBytes(buf, 9, 1);
		return buf;
	}

	public byte[] setMode(byte pMode) throws CommunicationException
	{
		final byte[] buf = buildCommand(CMD_MODE, new byte[] { 0x1, pMode });
		mPort.writeBytes(buf, buf.length);
		return readResponse();
	}

	public double[] queryData() throws CommunicationException
	{
		final byte[] buf = buildCommand(CMD_QUERY_DATA, new byte[0]);
		mPort.writeBytes(buf, buf.length);
		return readData(readResponse());
	}

	public void setWorkingPeriod(byte period) throws CommunicationException
	{
		final byte[] buf = buildCommand(CMD_WORKING_PERIOD, new byte[] { 0x1, period });
		mPort.writeBytes(buf, buf.length);
		readResponse();
	}

	public void setSleepMode(boolean pSleep) throws CommunicationException
	{
		final byte[] buf = buildCommand(CMD_SLEEP, new byte[] { 0x1, (byte)(pSleep ? 0 : 1) });
		mPort.writeBytes(buf, buf.length);
		readResponse();
		mSleeping = pSleep;
	}

	public FirmwareInfo getFirmwareInfo() throws CommunicationException
	{
		final byte[] buf = buildCommand(CMD_FIRMWARE, new byte[0]);
		mPort.writeBytes(buf, buf.length);
		mPort.flushIOBuffers();
		return readFirmwareInfo(readResponse());
	}

	public void close()
	{
		if (!mSleeping)
			try
			{
				setSleepMode(true);
			}
			catch (CommunicationException e)
			{
				// nothing much we can do here, we're closing everything
				System.err.println("Could not set the chip to sleep mode, make sure to disconnect it!");
			}
		mPort.closePort();
		mPort = null;
	}

	public void initQueryMode() throws CommunicationException
	{
		setWorkingPeriod(PERIOD_CONTINUOUS);
		setMode(MODE_QUERY);
	}

	public boolean isSleeping()
	{
		return mSleeping;
	}

	public static void main(String[] args) throws CommunicationException, InterruptedException, SerialPortInvalidPortException, SDS011CommunicationException
	{
		final SDS011Driver driver = new SDS011Driver(DEFAULT_PORT);
		Runtime.getRuntime().addShutdownHook(new Thread()
		{
			public void run()
			{
				try
				{
					Thread.sleep(200);
					System.out.println("Closing driver...");
					driver.close();
				}
				catch (InterruptedException e)
				{
					Thread.currentThread().interrupt();
					e.printStackTrace();
				}
			}
		});
		driver.setSleepMode(false);
		final FirmwareInfo firmwareInfo = driver.getFirmwareInfo();
		System.out.println(String.format("Firmware: %02d-%02d-%02d, ID: %s", firmwareInfo.year, firmwareInfo.month, firmwareInfo.day, firmwareInfo.id));
		driver.initQueryMode();
		while (true)
		{
			System.out.println("Waking up...");
			driver.setSleepMode(false);
			Thread.sleep(5000);
			for (int i = 0; i < 5; i++)
			{
				final double[] values = driver.queryData();
				System.out.println(String.format("PM2.5: %.1f, PM10: %.1f", values[0], values[1]));
				Thread.sleep(2000);
			}
			driver.setSleepMode(true);
			System.out.println("Sleeping for 5 minutes");
			Thread.sleep(60000 * 5);
		}
	}
}
